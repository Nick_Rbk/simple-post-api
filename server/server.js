const express = require('express'),
    bodyParser = require('body-parser');

const app = express(),
    port = process.env.PORT || 7777;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const apiRouter = express.Router();
app.use('/api', apiRouter);

apiRouter.route('/news')
// create a user (POST http://localhost:7777/api/news)
    .post( (req, res) => {
        console.log(req.body);
        res.json({ message: 'Congrats!', "your data": req.body });
    });



// START THE SERVER !)
// ==============================================================
app.listen(port, () => console.log(`Server is up on port ${port}`));
// ______________________________________________________________